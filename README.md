# static codereview SVN miscweb static websites

Blubber images for old codereview SVN static html sites.

HTML content is compressed (gz) in Git to decrease disk usage and git overhead. During the blubber image build everything gets uncompressed and copied to the container image.

## Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/`


## Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/static-codereview:<timestamp>`
